package BusquedaPatrones;

import java.util.ArrayList;

import javax.swing.text.TabableView;

import Texto.ITextoIterator;

public class BusquedaPatronesBoyerMoore implements IBusquedaPatrones {
	private ArrayList<Long>[] tablaCaracterFallido;
	private long[] tablaBuenSufijo;
	private ITextoIterator textoIterator;
	private ITextoIterator patronIterator;
	private long currentIndex;
	
	public BusquedaPatronesBoyerMoore(ITextoIterator textoIterator,
			ITextoIterator patronIterator) {
		super();
		this.textoIterator = textoIterator;
		this.patronIterator = patronIterator;
		
		this.hacerTablaBuenSufijo();
		this.hacerTablaCaracterFallido();
		this.resetSearch();
	}
	
	private void hacerTablaCaracterFallido() {
		ITextoIterator patronIteratorTmp = this.patronIterator.clone();
		patronIteratorTmp.reset();
		
		this.tablaCaracterFallido = new ArrayList[256];
		for( int i = 0; i < 256; i++ ) this.tablaCaracterFallido[i] = new ArrayList<Long>();
		
		while ( patronIteratorTmp.next() ) {
			this.tablaCaracterFallido[ patronIteratorTmp.getChar() ].add(
					patronIteratorTmp.getIndex() );
		}
	}
	
	private long obtenerIndiceCaracterFallido() {
		int indexTablaCaracterFallido =
				binarySearch(
						this.tablaCaracterFallido[ (int) this.textoIterator.getChar() ],
						this.patronIterator.getIndex() );
		
		if ( indexTablaCaracterFallido < 0 ) return -1;
		
		return this.tablaCaracterFallido[(int) this.textoIterator.getChar() ].get(
				indexTablaCaracterFallido );
				
	}
	
    private int binarySearch( ArrayList<Long> lista, Long elementoABuscar ) {
        return binarySearch(lista, elementoABuscar, 0, lista.size()-1);
    }	
    
    // busca un elemento en una lista, y si no esta devuelve el indice del elemento
    // existente de valor inferior
    private int binarySearch( ArrayList<Long> lista, Long elementoABuscar, int inicio, int fin ) {
        int low = inicio;
        int high = fin;
        int mid;
        int compareToResult;

        while( low <= high ) {
            mid = ( low + high ) / 2;

            compareToResult = lista.get( mid ).compareTo( elementoABuscar );
            if( compareToResult > 0 ) high = mid - 1;
            else if ( compareToResult < 0 ) low = mid + 1;
            else return mid;
        }

        return high;
    }	
    
	private void hacerTablaBuenSufijo() {
		ITextoIterator patronIteratorA = this.patronIterator.clone();
		ITextoIterator patronIteratorB = this.patronIterator.clone();
		
		this.tablaBuenSufijo = new long[ (int) patronIteratorA.getLength() ];
		this.tablaBuenSufijo[ (int) patronIteratorA.getLength() - 1 ] = patronIteratorA.getLength() - 2;
		
		
		long patSize = 1;
		long contCharEquals;
		int tablaBuenSufijoIndex = (int) patronIteratorA.getLength() - 2;
		
		// ponemos el indice del fichero al último caracter
		patronIteratorA.reset();
		patronIteratorA.back();
		
		patronIteratorB.reset();
		patronIteratorB.back();
		contCharEquals = patSize;
		
		while ( patronIteratorA.back() ) {
			while( ( contCharEquals > 0 ) &&
				   ( patronIteratorA.getChar() == patronIteratorB.getChar() ) ) {
				contCharEquals--;
				patronIteratorB.back();
				if ( !patronIteratorA.back() ) break;
			}
			
			if ( contCharEquals == 0 ) {
				this.tablaBuenSufijo[ tablaBuenSufijoIndex-- ] = patronIteratorA.getIndex();
				patronIteratorA.next();
				
				contCharEquals = 1;
				patSize++;
			} else {
				patronIteratorB.reset();
				patronIteratorB.back();
				
				contCharEquals = patSize;
			}
		}
		
		for ( int i = tablaBuenSufijoIndex; i >= 0; i-- )
			this.tablaBuenSufijo[ i ] = -1;
	}
	
	private long obtenerIndiceBuenSufijo() {
		return this.tablaBuenSufijo[ (int) this.patronIterator.getIndex() ];
	}

	@Override
	public boolean searchNext() {
		// TODO Auto-generated method stub
		this.textoIterator.moveToIndex( this.currentIndex + 2 * this.patronIterator.getLength() - 1 );
		
		this.patronIterator.reset();
		
		long newIndex;
		
		while ( this.patronIterator.back() ) {
			if ( this.textoIterator.getChar() == this.patronIterator.getChar() ) {
				this.textoIterator.back();
			} else {
				newIndex = Math.min(
						obtenerIndiceCaracterFallido(),
						obtenerIndiceBuenSufijo() );
				
				// si nos salimos del texto donde tenemos que buscar el patrón
				// significa que ya no hay ninguna ocurrencia del patrón en el texto
				if ( !this.textoIterator.moveToIndex(
						this.textoIterator.getIndex() +
						this.patronIterator.getLength() -
						newIndex - 1 ) ) {
					resetSearch();
					return false;
				}
				this.patronIterator.reset();
			}
		}
		
		this.currentIndex = this.textoIterator.getIndex() + 1;	
		return true;
	}

	@Override
	public void resetSearch() {
		// TODO Auto-generated method stub
		this.currentIndex = -this.patronIterator.getLength();
	}

	@Override
	public long currentPos() {
		// TODO Auto-generated method stub
		return this.currentIndex;
	}

}
