package BusquedaPatrones;

public interface IBusquedaPatrones {
	public long currentPos();
	public boolean searchNext();
	public void resetSearch();
}
