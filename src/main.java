import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.print.attribute.standard.Compression;

import BusquedaPatrones.BusquedaPatronesBoyerMoore;
import BusquedaPatrones.IBusquedaPatrones;
import Texto.FileIterator;
import Texto.ITextoIterator;
import Texto.RandomIterator;
import Texto.StringIterator;

public class main {
	public static void main(final String[] args) {
		Scanner scanner = new Scanner( System.in );
		
		ITextoIterator textoIterator, patronIterator;
		
		String filename, patron;
		double porcentaje;
		int numSeparaciones;
		char opcion;
		
		System.out.println ("####################################################################");
		System.out.println ("#                Practica 4 de Diseno de Algoritmos                #");
		System.out.println ("# ---------------------------------------------------------------- #");
		System.out.println ("#   Conteo del numero de correspondencias de un patron en un       #");
		System.out.println ("#   texto tanto del modo determinista como probabilistico          #");
		System.out.println ("####################################################################");
		System.out.println ("");
		

		do {			
			try {
				System.out.println("Introduzca la ruta del fichero del que quiere contar las ocurrencias:");
				filename = scanner.next();
				
				System.out.println("Introduzca el patron que desea contar:");
				patron = scanner.next();
				
				System.out.println("Introduzca el porcentaje del texto que desea analizar probabilisticamente:");
				porcentaje = scanner.nextDouble();
				
				System.out.println("Intoduzca el numero de separaciones del porcentaje a analizar:");
				numSeparaciones = scanner.nextInt();
				
				textoIterator = new FileIterator( filename );
				patronIterator = new StringIterator( patron );
				
				System.out.println("");
				System.out.println("Resultados de los analisis:");
	
				System.out.printf("  Metodo determinista: %d ocurrencias\n",
						contarBoyerMooreDeterminista(textoIterator, patronIterator) );
				System.out.printf("  Metodo probabilista: %d ocurrencias\n",
						contarBoyerMooreProbabilista(textoIterator, patronIterator, porcentaje, numSeparaciones) );
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("");
			System.out.println ("Desea continuar? [y,N]");
			opcion = scanner.next().charAt(0);
			System.out.println("");
			
		} while ((opcion == 'Y') || (opcion == 'y'));
	}	
	
	public static int contarBoyerMooreDeterminista(ITextoIterator texto, ITextoIterator patron) {
		IBusquedaPatrones busq = new BusquedaPatronesBoyerMoore(texto, patron);
			
		int cont = 0;
		while( busq.searchNext() ) cont++;
			
		return cont;
	}	
	
	public static int contarBoyerMooreProbabilista(ITextoIterator texto, ITextoIterator patron,
			double porcentajeAAnalizar, int numeroDeSeparaciones ) {
		ITextoIterator textoAleatorio = new RandomIterator(texto, porcentajeAAnalizar, numeroDeSeparaciones);
		
		return (int) ( contarBoyerMooreDeterminista(textoAleatorio, patron) * ( 100.0 / porcentajeAAnalizar ) );
	}	
}
