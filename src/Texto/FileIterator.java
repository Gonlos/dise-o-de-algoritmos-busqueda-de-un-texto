package Texto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileIterator implements ITextoIterator {
	private int BUFFER_SIZE = 8192;
	private char[] buffer = new char[BUFFER_SIZE];
	
	private long index;
	private long bufferIndex;
	
	private String filename;
	private File file;
	private FileInputStream fileStream;
	
	public FileIterator(String filename) throws FileNotFoundException {
		super();
		
		this.filename = filename;
	    this.file = new File( this.filename );
	    this.fileStream = new FileInputStream( this.file );
	    
	    this.index = -1;
	    this.bufferIndex = - this.BUFFER_SIZE;
	}

	@Override
	public boolean next() {
		// TODO Auto-generated method stub
		if ( this.index == getLength()-1 ) {
			this.index = -1;
			return false;
		}
		
		this.index++;
		return true;
	}

	@Override
	public boolean back() {
		// TODO Auto-generated method stub
		if ( this.index == 0 ) {
			this.index = -1;
			return false;
		}
		
		if ( this.index == -1 ) this.index = getLength();
		
		this.index--;
		return true;
	}
	
	private void readBuffer() {
		this.bufferIndex = Math.max(0, this.index - this.BUFFER_SIZE / 2);
		
		try{
			this.fileStream.close();
			this.fileStream = new FileInputStream( this.file );
			this.fileStream.skip( this.bufferIndex );
		
			long l = Math.min(getLength()-this.bufferIndex, this.BUFFER_SIZE);
			for( int i = 0; i < l; i++ )
				this.buffer[ i ] = (char) this.fileStream.read();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}	

	@Override
	public char getChar() {
		// TODO Auto-generated method stub
		if ( ( this.index < this.bufferIndex ) ||
			 ( this.bufferIndex + this.BUFFER_SIZE - 1 < this.index ) )
			readBuffer();
		
		return this.buffer[ (int) ( this.index - this.bufferIndex ) ];
	}

	@Override
	public long getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public boolean moveToIndex(long index) {
		// TODO Auto-generated method stub
		if ( (0 <= index) && (index < getLength())) {
			this.index = index;
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		this.index = -1;
	}

	@Override
	public long getLength() {
		// TODO Auto-generated method stub
		return this.file.length();
	}

	@Override
	public ITextoIterator clone() {
		// TODO Auto-generated method stub
		ITextoIterator it = null;
		
		try {
			it = new FileIterator( this.filename );
			it.moveToIndex( getIndex() );
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return it;
	}
	
	protected void finalize ()  {
        try {
			this.fileStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

}
