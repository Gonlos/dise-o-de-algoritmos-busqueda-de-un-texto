package Texto;

public class StringIterator implements ITextoIterator {
	private String texto;
	private long textoIndex;
	
	public StringIterator(String texto) {
		super();
		this.texto = texto;
		this.textoIndex = -1;
	}

	@Override
	public boolean next() {
		// TODO Auto-generated method stub
		this.textoIndex++;
		
		if ( this.textoIndex >= this.texto.length() ) {
			this.textoIndex = -1;
			return false;
		} else {
			return true;
		}
	}

	@Override
	public boolean back() {
		// TODO Auto-generated method stub
		this.textoIndex--;
		
		if ( this.textoIndex < 0 ) {
			if ( this.textoIndex == -2 ) {
				this.textoIndex = this.texto.length() - 1;
				return this.textoIndex >= 0;
			} else {
				this.textoIndex = -1;
				return false;
			}
		} else {
			return true;
		}
	}

	@Override
	public char getChar() {
		// TODO Auto-generated method stub
		return this.texto.charAt((int) this.textoIndex );
	}

	@Override
	public long getIndex() {
		// TODO Auto-generated method stub
		return this.textoIndex;
	}

	@Override
	public boolean moveToIndex(long index) {
		// TODO Auto-generated method stub
		this.textoIndex = index;
		
		if ( (this.textoIndex < 0) || (this.textoIndex >= this.texto.length()) ) {
			this.textoIndex = -1;
			return false;
		} else {
			return true;
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		this.textoIndex = -1;
	}

	@Override
	public long getLength() {
		// TODO Auto-generated method stub
		return this.texto.length();
	}

	@Override
	public ITextoIterator clone() {
		// TODO Auto-generated method stub
		ITextoIterator it = new StringIterator( this.texto );
		it.moveToIndex( this.textoIndex );
		
		return it;
	}

}
