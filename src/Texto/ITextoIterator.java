package Texto;

public interface ITextoIterator {
	public boolean next();
	public boolean back();
	public char getChar();
	public long getIndex();
	public boolean moveToIndex( long index );
	public void reset();
	public long getLength();
	public ITextoIterator clone();
}
