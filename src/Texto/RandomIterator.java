package Texto;

import java.util.Arrays;
import java.util.Random;

public class RandomIterator implements ITextoIterator {
	private ITextoIterator iterator;
	private long[] indicesIniciales;
	private long[] indicesFinales;
	private long[] longitudesAcumuladas;
	private int indiceActualArray;
	private long indiceIterador;
	private long longitud;
	private boolean estoyReset;
	
	public RandomIterator(ITextoIterator iterator, double porcentajeAAnalizar,
			int numSeparaciones ) {
		super();
		this.iterator = iterator;
		this.longitud = (long) ( this.iterator.getLength() * ( porcentajeAAnalizar / 100.0) );
		
	    Random rand = new Random();
	    long posiblesIndices = this.iterator.getLength() - this.longitud - ( numSeparaciones - 1);
	    long posiblesIndicesVirtuales = this.longitud - ( numSeparaciones - 1);
	    
	    this.indicesIniciales = new long[numSeparaciones];
	    this.indicesFinales = new long[numSeparaciones];
	    this.longitudesAcumuladas = new long[numSeparaciones + 1];
	    long[] indicesVirtuales = new long[numSeparaciones + 1];
	    for ( int i = 0; i < numSeparaciones; i++ ) {
	    	this.indicesIniciales[i] = Math.abs( rand.nextLong() % posiblesIndices );
	    	indicesVirtuales[i] = Math.abs( rand.nextLong() % posiblesIndicesVirtuales );
	    }
	    indicesVirtuales[0] = 0;
	    indicesVirtuales[numSeparaciones] = posiblesIndicesVirtuales+1;
	    
	    Arrays.sort( this.indicesIniciales );
	    Arrays.sort( indicesVirtuales );
	    
	    long espacioYaAsignado = 0;
	    long nuevoEspacio;
    	this.longitudesAcumuladas[0] = 0;
	    for ( int i = 0; i < numSeparaciones; i++ ) {
	    	this.indicesIniciales[i] += espacioYaAsignado;
	    	nuevoEspacio = indicesVirtuales[i+1] - indicesVirtuales[i];
	    	this.indicesFinales[i] = this.indicesIniciales[i] + nuevoEspacio - 1;
	    	espacioYaAsignado += nuevoEspacio;
	    	this.longitudesAcumuladas[i+1] = espacioYaAsignado;
	    }
    	
    	this.estoyReset = true;
    	this.indiceIterador = -1;
	}
	
	// constructor para el clone
	private RandomIterator(ITextoIterator iterator, long[] indicesIniciales,
			long[] indicesFinales, long[] longitudesAcumuladas,
			int indiceActualArray, long indiceIterador, long longitud,
			boolean estoyReset) {
		super();
		this.iterator = iterator;
		this.indicesIniciales = indicesIniciales;
		this.indicesFinales = indicesFinales;
		this.longitudesAcumuladas = longitudesAcumuladas;
		this.indiceActualArray = indiceActualArray;
		this.indiceIterador = indiceIterador;
		this.longitud = longitud;
		this.estoyReset = estoyReset;
	}
	
	@Override
	public boolean next() {
		if ( this.estoyReset ) {
	    	this.indiceIterador = 0;
		    this.indiceActualArray = 0;
		    this.estoyReset = !this.iterator.moveToIndex( this.indicesIniciales[ this.indiceActualArray ] );
		    return !this.estoyReset;
		}
		
		this.indiceIterador++;
		if ( this.indicesFinales[ this.indiceActualArray ] <= this.iterator.getIndex() ) {
			this.indiceActualArray++;
			if ( this.indiceActualArray < this.indicesIniciales.length ) {
				this.estoyReset = !this.iterator.moveToIndex( this.indicesIniciales[ this.indiceActualArray ] );
			} else {
				this.estoyReset = true;
			}
			
			return !this.estoyReset;
		}
		
		return this.iterator.next();
	}
	
	@Override
	public boolean back() {
		if ( this.estoyReset ) {
	    	this.indiceIterador = this.longitud-1;
		    this.indiceActualArray = this.indicesIniciales.length - 1;
		    this.estoyReset = !this.iterator.moveToIndex( this.indicesFinales[ this.indiceActualArray ] );
		    return !this.estoyReset;
		}
		
		this.indiceIterador--;
		if ( this.indicesIniciales[ this.indiceActualArray ] >= this.iterator.getIndex() ) {
			this.indiceActualArray--;
			if ( this.indiceActualArray >= 0 ) {
				this.estoyReset = !this.iterator.moveToIndex( this.indicesFinales[ this.indiceActualArray ] );
			} else {
				this.estoyReset = true;
			}
			
			return !this.estoyReset;
		}
		
		return this.iterator.back();
	}
	
	@Override
	public char getChar() {
		return this.iterator.getChar();
	}
	
	@Override
	public long getIndex() {
		return this.indiceIterador;
	}
	
	@Override
	public boolean moveToIndex(long index) {
		if ( ( index < this.longitudesAcumuladas[ this.indiceActualArray ] ) ||
			 ( this.longitudesAcumuladas[ this.indiceActualArray + 1 ] <= index ) ) {
			
			int nuevoIndice = binarySearch( this.longitudesAcumuladas, index );
			
			if ( ( nuevoIndice < 0 ) ||
				 ( nuevoIndice >= this.indicesIniciales.length ) ) return false;
			this.indiceActualArray = nuevoIndice;
		}
		
		this.estoyReset = false;
		this.indiceIterador = index;
		return this.iterator.moveToIndex(
				this.indicesIniciales[ this.indiceActualArray ] +
				( index - this.longitudesAcumuladas[ this.indiceActualArray ] ) );
	}
	

    private int binarySearch( long[] lista, long elementoABuscar ) {
        return binarySearch(lista, elementoABuscar, 0, lista.length-1);
    }	
    
    // busca un elemento en una lista, y si no esta devuelve el indice del elemento
    // existente de valor inferior
    private int binarySearch( long[] lista, long elementoABuscar, int inicio, int fin ) {
        int low = inicio;
        int high = fin;
        int mid;
        long compareToResult;

        while( low <= high ) {
            mid = ( low + high ) / 2;

            compareToResult = lista[ mid ] - elementoABuscar;
            if( compareToResult > 0 ) high = mid - 1;
            else if ( compareToResult < 0 ) low = mid + 1;
            else return mid;
        }

        return high;
    }	
	
	@Override
	public void reset() {
		this.estoyReset = true;
	}
	
	@Override
	public long getLength() {
		return this.longitud;
	}
	
	@Override
	public ITextoIterator clone() {
		ITextoIterator it = new RandomIterator(
				this.iterator, this.indicesIniciales, this.indicesFinales,
				this.longitudesAcumuladas, this.indiceActualArray,
				this.indiceIterador, this.longitud, this.estoyReset);
		
		return it;
	}
}
